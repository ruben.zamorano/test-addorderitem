from msc import microservice, db as mongo
import jsonschema,datetime
import json
import time,os
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

config = {}
config_serv = {}
ENDPOINT_PARAMS = {}
    
def iniciar(config,jsonArguments):
   
    response = {}
    argumentos = jsonArguments
    initConfigEndpoint()

    if(len(argumentos)>0
        and "WCToken" in argumentos 
        and "WCTrustedToken" in argumentos 
        and "orderItem" in argumentos 
        and len(argumentos["orderItem"]) > 0):
        if("storeId" in argumentos):
            storeId = str(argumentos["storeId"])
        else:
            storeId = str(ENDPOINT_PARAMS["storeId"])
        respuesta = addOrderItem(storeId,argumentos)
        
        return(0,respuesta["data"])
    else:
    #regresar errores
        return(-2,{})

def addOrderItem(storeId,argumentos):
    headers = {
        "Content-Type":"application/json",
        "WCToken":argumentos["WCToken"],
        "WCTrustedToken":argumentos["WCTrustedToken"]
    }

    validate = 1
    for orderItem in argumentos["orderItem"]:
        if("productId" not in orderItem or "quantity" not in orderItem):
            validate = 0

    if(validate==1):
        datos = {"orderItem" : argumentos["orderItem"]}
        url = ENDPOINT_PARAMS["wcs_endpoint"] + storeId + "/" +  config_serv["wcsparams"]["resource"]
        proxy = ""

        print("/////////////////////////////////////////////////////////////////")
        print("WCToken")
        print(argumentos["WCToken"])
        print("url:")
        print(url)
        print("body:")
        print(datos)
        print("/////////////////////////////////////////////////////////////////")
        if("proxies" in config_serv):
            proxy = config_serv["proxies"]
        r = requests.post(str(url),
                            headers=headers,
                            data=json.dumps(datos),
                            verify=False,
                            proxies=proxy,
                            timeout=int(config["timeout_internos"]))
        res = {"status":"SUCCESS","data":""}
        if(r.status_code != 201):
            res["status"] = "ERROR"
        res["data"] = r.json()
        
    return res

def logica_servicio(config, jsonArguments, llamarWorker):
    return iniciar(config,jsonArguments["data"])

def initConfigEndpoint():
        global ENDPOINT_PARAMS
        # print('--------------------------------')
        # print(config_serv["wcsparams"])
        # print('--------------------------------')
        if ("environment" in config and config["environment"] in config):
            ENDPOINT_PARAMS = config[config["environment"]]
        else:
            ENDPOINT_PARAMS = { 
                "search_endpoint" : config_serv["wcsparams"]["endpoint"], 
                "storeId" : config_serv["wcsparams"]["storeId"] 
            }	
    
        # "_zookeeperServer": '10.44.16.31:9092',

# Inicializa la libreria de microservicios coppel
ms = microservice({
    "_debug":True,
    "_type":"worker",
    "_zookeeperServer": '10.142.0.2:9092',
    "_appName":"appcoppel",
    "_versionName":"v1",
    "_actionName":"addOrderItemWCS"
})
# En este metodo se agregan los errores que se manejaran en tu funcion de logica.
ms.setErrors({
    "-6":"No se encontro informacion del cliente"
})
# En este metodo se las deelsependencias externas ejemplo. una url de un servicio externo
MONGO = os.environ.get("MONGO_URI")
conexion = mongo(MONGO).conexion()
db = conexion["configuraciones_appcom"]
col_miColeccion = db["configuraciones"]
datos = col_miColeccion.find_one({"_id":"addOrderItemWCS"})
datos2 = col_miColeccion.find_one({"_id":"general"})
datos["configuracion"]["mongoUri"] = MONGO
config = datos2["configuracion"]
config_serv = datos["configuracion"]
# Metodo que arranca el servicio.



ms.start(logica_servicio,{})